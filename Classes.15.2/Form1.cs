﻿using System;
using System.Windows.Forms;

namespace Classes._15._2
{
    public partial class Form1 : Form
    {
        private Student s1;

        public Form1()
        {
            InitializeComponent();
        }

        private void btnToon_Click(object sender, EventArgs e)
        {
            s1 = new Student();

            if (!double.TryParse(tbNaam.Text, out double x))
            {
                s1.Naam = tbNaam.Text;
            }
            else
            {
                MessageBox.Show("Geen geldige waarde in vak naam!", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            //wiskunde
            if (double.TryParse(tbWiskunde.Text, out double wisk) && !string.IsNullOrWhiteSpace(tbWiskunde.Text))
            {
                s1.Wiskunde = wisk;
            }
            else
            {
                MessageBox.Show("Geen geldige waarde in vak wiskunde!", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            //informatica
            if (double.TryParse(tbInformatica.Text, out double result) || !string.IsNullOrWhiteSpace(tbInformatica.Text))
            {
                s1.Informatica = result;
            }

            else
            {
                MessageBox.Show("Geen geldige waarde in vak informatica!", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            //uitvoer
            if (string.IsNullOrWhiteSpace(tbNaam.Text) || string.IsNullOrWhiteSpace(tbWiskunde.Text) || string.IsNullOrWhiteSpace(tbInformatica.Text) || double.TryParse(tbNaam.Text, out double r))
            {
                tbOut.Text = "Geen uitvoer";
            }
            else
            {
                tbOut.Text = s1.ToonGegevens();
            }
        }

        private void tbNaam_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbWiskunde_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbInformatica_TextChanged(object sender, EventArgs e)
        {
        }

        private void tbOut_TextChanged(object sender, EventArgs e)
        {
        }
    }
}