﻿using System.Windows.Forms;

namespace Classes._15._2
{
    internal class Student
    {
        private double _informatica, _wiskunde;
        private string _naam;

        public Student()
        {
            this.Informatica = 0;
            this.Wiskunde = 0;
            this.Naam = "(Naam is leeg)";
        }

        public double Informatica
        {
            get { return _informatica; }
            set
            {
                if (!(value < 0 || value > 100))
                {
                    _informatica = value;
                }
                else
                {
                    MessageBox.Show("Geen geldige waarde in vak informatica!", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        public double Wiskunde
        {
            get { return _wiskunde; }
            set
            {
                if (!(value < 0 || value > 100))
                {
                    _wiskunde = value;
                }
                else
                {
                    MessageBox.Show("Geen geldige waarde in vak wiskunde!", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        public string Naam
        {
            get { return _naam; }
            set
            {
                if (!string.IsNullOrWhiteSpace(value) )
                {
                    _naam = value;
                }
                else
                {
                    MessageBox.Show("Geen geldige naam!", "Error 404", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
        }

        public string ToonGegevens()
        {
            return Naam + " heeft " + Wiskunde + " voor wiskunde en " + Informatica + " voor informatica behaald!";
        }
    }
}