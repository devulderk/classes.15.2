﻿namespace Classes._15._2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnToon = new System.Windows.Forms.Button();
            this.tbNaam = new System.Windows.Forms.TextBox();
            this.tbWiskunde = new System.Windows.Forms.TextBox();
            this.tbInformatica = new System.Windows.Forms.TextBox();
            this.tbOut = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnToon
            // 
            this.btnToon.Location = new System.Drawing.Point(331, 12);
            this.btnToon.Name = "btnToon";
            this.btnToon.Size = new System.Drawing.Size(149, 63);
            this.btnToon.TabIndex = 0;
            this.btnToon.Text = "Toon Rapport";
            this.btnToon.UseVisualStyleBackColor = true;
            this.btnToon.Click += new System.EventHandler(this.btnToon_Click);
            // 
            // tbNaam
            // 
            this.tbNaam.Location = new System.Drawing.Point(83, 12);
            this.tbNaam.Name = "tbNaam";
            this.tbNaam.Size = new System.Drawing.Size(184, 20);
            this.tbNaam.TabIndex = 1;
            this.tbNaam.TextChanged += new System.EventHandler(this.tbNaam_TextChanged);
            // 
            // tbWiskunde
            // 
            this.tbWiskunde.Location = new System.Drawing.Point(83, 38);
            this.tbWiskunde.Name = "tbWiskunde";
            this.tbWiskunde.Size = new System.Drawing.Size(184, 20);
            this.tbWiskunde.TabIndex = 2;
            this.tbWiskunde.TextChanged += new System.EventHandler(this.tbWiskunde_TextChanged);
            // 
            // tbInformatica
            // 
            this.tbInformatica.Location = new System.Drawing.Point(83, 64);
            this.tbInformatica.Name = "tbInformatica";
            this.tbInformatica.Size = new System.Drawing.Size(184, 20);
            this.tbInformatica.TabIndex = 3;
            this.tbInformatica.TextChanged += new System.EventHandler(this.tbInformatica_TextChanged);
            // 
            // tbOut
            // 
            this.tbOut.Location = new System.Drawing.Point(83, 139);
            this.tbOut.Multiline = true;
            this.tbOut.Name = "tbOut";
            this.tbOut.Size = new System.Drawing.Size(397, 149);
            this.tbOut.TabIndex = 4;
            this.tbOut.TextChanged += new System.EventHandler(this.tbOut_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(38, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Naam:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Wiskunde: ";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 67);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(65, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Informatica: ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 142);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(48, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Rapport:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(511, 303);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.tbOut);
            this.Controls.Add(this.tbInformatica);
            this.Controls.Add(this.tbWiskunde);
            this.Controls.Add(this.tbNaam);
            this.Controls.Add(this.btnToon);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnToon;
        private System.Windows.Forms.TextBox tbNaam;
        private System.Windows.Forms.TextBox tbWiskunde;
        private System.Windows.Forms.TextBox tbInformatica;
        private System.Windows.Forms.TextBox tbOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
    }
}

